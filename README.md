## raven-user 12 SQ3A.220605.009.B1 8650216 release-keys
- Manufacturer: google
- Platform: gs101
- Codename: raven
- Brand: google
- Flavor: raven-user
- Release Version: 12
- Id: SQ3A.220605.009.B1
- Incremental: 8650216
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-US
- Screen Density: undefined
- Fingerprint: google/raven/raven:12/SQ3A.220605.009.B1/8650216:user/release-keys
- OTA version: 
- Branch: raven-user-12-SQ3A.220605.009.B1-8650216-release-keys
- Repo: google_raven_dump_2299


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
